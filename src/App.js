import React from 'react';
import Form from './components/Form/Form';
import ListWrapper from './components/ListWrapper/ListWrapper';
import './index.css';

const initialStateItems = [
  {
    image: 'https://thumbs.dreamstime.com/z/happy-smiling-geek-hipster-beard-man-cool-avatar-geek-man-avatar-104871313.jpg',
    name: 'Dan Abramov',
    description: 'React framework creator, JavaScript development visioner, who announced React creation on the Silicon Valley convention in 2001.',
    twitterLink: 'https://twitter.com/dan_abramov',
},
{
    image: 'https://previews.123rf.com/images/anwarsikumbang/anwarsikumbang1502/anwarsikumbang150200446/36649713-man-avatar-user-picture-cartoon-character-vector-illustration.jpg',
    name: 'Ryan Florence',
    description: 'Making React accessible for users and developers at https://reach.tech . Online learning, workshops, OSS, and consulting.',
    twitterLink: 'https://twitter.com/ryanflorence',
},
{
    image: 'https://previews.123rf.com/images/yupiramos/yupiramos1712/yupiramos171217688/92110199-elegant-businessman-avatar-character-vector-illustration-design.jpg',
    name: 'Michael Jackson',
    description: 'Maker. Co-author of React Router. Working on @ReactTraining. Created @unpkg. Head over heels for @cari.',
    twitterLink: 'https://twitter.com/mjackson',
},
{
    image: 'https://previews.123rf.com/images/yupiramos/yupiramos1712/yupiramos171217686/92110197-elegant-businessman-avatar-character-vector-illustration-design.jpg',
    name: 'Kent C. Dodds',
    description: 'Making software development more accessible · Husband, Father, Latter-day Saint, Teacher, OSS, GDE, @TC39 · @PayPalEng @eggheadio @FrontendMasters · #JS',
    twitterLink: 'https://twitter.com/kentcdodds',
},
];

class App extends React.Component {

  state = {
    items: [...initialStateItems],
  };

  addItem = (event) => {
    event.preventDefault();

    const newItem = {
      name: event.target[0].value,
      twitterLink: event.target[1].value,
      image: event.target[2].value,
      description: event.target[3].value,
    }

    this.setState (prevState => ({
      items: [...prevState.items, newItem],
    }));

    event.target.reset();

  } 

  render() {
    return (
      <div>
        <ListWrapper
          items={this.state.items}   
        />
        <Form submitFunction={this.addItem}/>
      </div>
    );
  }
};

export default App;