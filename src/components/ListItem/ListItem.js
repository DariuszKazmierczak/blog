import React from 'react';
import './ListItem.css';
import PropTypes from 'prop-types';

const ListItem = ({
    name, 
    image, 
    description, 
    twitterLink
}) => (
    <li className='listItem__wrapper'>
            <img 
                className="listItem__image" 
                src={image}
                alt={name}
             />
            <div >
                <h2 
                    className="listItem__name">
                        {name}
                </h2>
                <p 
                    className="listItem__description">
                    {description}
                </p>
                <a 
                    className="listItem__button" 
                    href={twitterLink}>
                    visit Twitter Page
                </a>
        </div>
    </li>
);

ListItem.propTypes = {
    name: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    description: PropTypes.string,
    twitterLink: PropTypes.string.isRequired,
};

ListItem.defaultProps = {
    description: 'One of the React creators'
};

export default ListItem;